# GitHub Profile Finder

A simple app ment to fetch for the GitHub users by their usernames and to see their latest 5 repos and other info. 

### Prerequisites

none

### Installing

git clone https://github.com/letowebdev/Github-Profile-Finder.git

## Deployment

https://zacheleto.me/Projects/githubprofilefinder

## Built With

* [JavaScript ES6, ES7]
* [GitHub API]

## Author

* **Zache Abdelatif (Leto)** 

## License

This project is licensed under the MIT License
