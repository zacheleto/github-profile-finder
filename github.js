class Github{

constructor()
{
    this.client_id = '64df075a1108eb78226f';
    this.client_secret = 'a8fef986debf7032d9591ca3f917b275dcee10db';
    this.repos_count = 5;
    this.repos_sort = 'created: asc';
}

async getUser(user)
{
    const profileResponse = await fetch(`https://api.github.com/users/${user}?client_id=${this.client_id}&client_secret=${this.client_secret}`);

    const repoResponse = await fetch(`https://api.github.com/users/${user}/repos?per_page=${this.repos_count}&sort=${this.repos_sort}&client_id=${this.client_id}&client_secret=${this.client_secret}`);

    const profile = await profileResponse.json();
    const repos = await repoResponse.json();
    return {
        profile,
        repos
    };
}











}